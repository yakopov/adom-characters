# ADOM characters

Saved games from the best rogue-like RPG [ADOM](https://www.adom.de/home/index.html) - I've been playing this game for above 15 years and it never gets old.

As a proper rogue-like game, every character can only die once - so this is an experiment of keeping a history of the savegame file.